function createCard(name, description, pictureUrl,start_date,end_date,location_place) {
    return `
      <div class = "col-sm-4 mb-auto mx-auto my-auto">

        <div class = "card shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-location_place">${location_place}</h6>
          <p class="card-text">${description}</p>
          <footer class = "card-footer">${start_date}-${end_date}</footer>
        </div>
      </div>
    `;
  }

window.addEventListener('DOMContentLoaded',async () => {

        const url = 'http://localhost:8000/api/conferences/';
        try{
            const response = await fetch(url);
            if(!response.ok){
                console.log("something broke");
            } else{
            const data = await response.json();

            for(let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok){
                    const detail = await detailResponse.json();
                    const name = detail.conference.name;
                    const description = detail.conference.description;
                    const pictureUrl = detail.conference.location.picture_url;
                    const start_date = (new Date(detail.conference.starts).toDateString());
                    const end_date = (new Date(detail.conference.ends).toDateString());
                    const location_place = detail.conference.location.name;
                    const html = createCard(name, description, pictureUrl,start_date,end_date,location_place);
                    const column = document.querySelector('.row');

                    // const column = document.querySelector('');
                    column.innerHTML += html;
                    console.log(html)

            }
        }

        }
    }   catch (error) {
          console.error("Theres been an error loading location")
    }
});
